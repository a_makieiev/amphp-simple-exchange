<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:27
 */

namespace src\Exceptions;


use Amp\Http\Server\Response;
use Amp\Http\Status;
use src\Helper\ConstantsHelper;
use src\Interfaces\ExceptionsResponseInterfaces;

class ExceptionsResponse implements ExceptionsResponseInterfaces
{

	public static function badRequest($massage)
	{
		return new Response(Status::BAD_REQUEST, [
			"content-type" => "text/plain; charset=utf-8"
		], $massage);
	}

	public static function methodNotAllow($massage)
	{
		return new Response(Status::METHOD_NOT_ALLOWED, [
			"content-type" => "text/plain; charset=utf-8"
		], $massage);
	}

	public static function serverError($massage)
	{
		return new Response(Status::INTERNAL_SERVER_ERROR, [
			"content-type" => "text/plain; charset=utf-8"
		], $massage);
	}

	public static function pageNotFound($massage)
	{
		return new Response(Status::INTERNAL_SERVER_ERROR, [
			"content-type" => "text/plain; charset=utf-8"
		], $massage);
	}

	public static function responseWithExeption(\Exception $exception){
		switch ($exception->getMessage()){
			case ConstantsHelper::METHOD_NOT_ALLOW : {
				return self::methodNotAllow(ConstantsHelper::METHOD_NOT_ALLOW);
			}
		}
	}
}