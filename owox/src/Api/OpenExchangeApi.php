<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:41
 */

namespace src\Api;


use src\Interfaces\ApiInterface;

class OpenExchangeApi implements ApiInterface
{
	private const API_KEY = 'f836e1c5870e4d9eb529cf45b4f46534';
	private const BASE_URL = 'https://openexchangerates.org/api';
	private static $instance;
	private $apiKey;
	private $baseUrl;


	private function __construct($baseUrl, $apiKey)
	{
		$this->baseUrl = $baseUrl;
		$this->apiKey = $apiKey;
	}

	public static function getInstance()
	{
		if (NULL === self::$instance) {
			self::$instance = new self(self::BASE_URL, self::API_KEY);
		}
		return self::$instance;
	}

	public function getUrlCurrencyExchange(string $baseCurrency = 'USD')
	{
//		https://openexchangerates.org/api/latest.json?app_id=f836e1c5870e4d9eb529cf45b4f46534&base=BTC
		return $this->baseUrl . '/latest.json?app_id='.$this->apiKey.'&base='.$baseCurrency;
	}

	public function getUrlCurrencyHistoricalExchange(string $data,string $baseCurrency = 'USD')
	{
		return $this->baseUrl . '/historical/'.$data.'.json?app_id='.$this->apiKey.'&base='.$baseCurrency;
	}
}