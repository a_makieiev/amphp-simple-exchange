<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 19:29
 */

namespace src\Validators;


abstract class AbstractValidator
{
	/**
	 * @param        $input
	 * @param string $name
	 * @param bool   $required
	 * @return float
	 * @throws \Exception
	 */
	public function validateFloat($input, string $name, bool $required = TRUE)
	{

		if ($required) $this->validateOrBadRequest($input, $name);
		$input = doubleval($input);
		if (!is_double($input)) throw  new \Exception($name . ' must be float/int');
		if ($input == 0) {
			throw  new \Exception($name . ' must be float/int and non zero');
		}
		return doubleval($input);

	}

	/**
	 * @param        $input
	 * @param string $name
	 * @return mixed
	 * @throws \Exception
	 */

	public function validateOrBadRequest($input, string $name)
	{
		if (isset($input)) {
			return $input;
		} else {
			throw new \Exception($name . ' is require');
		}
	}

	/**
	 * @param        $input
	 * @param string $name
	 * @param bool   $required
	 * @return mixed
	 * @throws \Exception
	 */
	public function validateString($input, string $name, bool $required = TRUE)
	{
		if ($required) {
			$this->validateOrBadRequest($input, $name);

			if (!is_string($input)) throw new \Exception($name . ' must be string');
			return $input;
		}else{
			return $input;
		}
	}
}