<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:52
 */

namespace src\Validators;


use Amp\Http\Server\Request;

abstract class AbstractActionValidator extends AbstractValidator
{
	/**
	 * @param Request $request
	 * @param         $method
	 * @throws \Exception
	 */
	public function validateMethod(Request $request, $method): void
	{

		if ($request->getMethod() != $method)
			throw new \Exception('Method not Allow');
	}
}