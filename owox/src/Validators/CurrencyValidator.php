<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:39
 */

namespace src\Validators;


use DateTime;

class CurrencyValidator extends AbstractActionValidator
{
	/**
	 * @param        $input
	 * @param string $name
	 * @return string
	 * @throws \Exception
	 */
	public function validateCurrency($input, string $name)
	{

		$this->validateString($input, $name);
		if (!(strlen($input) == 3)) throw new \Exception('Length must be 3 letter');
		return $input;
	}

	/**
	 * @param $input
	 * @param $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function validateData($input, $name)
	{
		$this->validateString($input, $name, FALSE);
		$dt = DateTime::createFromFormat("Y-m-d", $input);
		if ($dt !== FALSE && !array_sum($dt->getLastErrors())) {
			return $input;
		};
	}


}