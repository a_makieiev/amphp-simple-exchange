<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:35
 */

namespace src\Action;


use Amp\Artax\DefaultClient;
use Amp\Deferred;
use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Status;
use src\Api\OpenExchangeApi;
use src\Helper\ConvertHelper;
use src\Helper\CurrencyCalculateHelper;
use src\Helper\RequestHelper;
use src\Validators\CurrencyValidator;
use function Amp\call;

class CurrencyAction
{

	public const ALLOW_METHOD = 'GET';

//	Переменная запроса для получение параметров.
	private $request;
//	Данные для данное метода.
	private $currencyFrom;
	private $currencyTo;
	private $amount;
	private $date;
//	Класс для валидации данных.
	private $validator;
//	Переменная для внешнего апи которое будет использовано для запроса.
	private $API;

	public function __construct(Request $request)
	{
		$this->validator = new CurrencyValidator();
		$this->request = $request;
		$this->API = OpenExchangeApi::getInstance();
	}

	public function run()
	{
		if ($this->validateParams() instanceof Response) return $this->validateParams();
		var_dump($this->date);
		if ($this->date) {

			return $this->actionHistoricalExchange();
		} else {

			return $this->actionExchange();
		}
	}

	public function validateParams()
	{

		try {
			$arrayParams = RequestHelper::getParams($this->request);
			var_dump($arrayParams);
			$this->validator->validateMethod($this->request, $this::ALLOW_METHOD);
			$this->currencyFrom = $this->validator->validateCurrency(($arrayParams['from']), 'from');
			$this->currencyTo = $this->validator->validateCurrency(($arrayParams['to']), 'to');
			if ($this->currencyFrom == $this->currencyTo) {
				throw new \Exception('Currency must be different');
			}
			$this->amount = $this->validator->validateFloat(($arrayParams['amount']), 'amount');
			$this->date = $this->validator->validateData(($arrayParams['date']), 'date');
		} catch (\Exception $exception) {
			return new Response(
				Status::BAD_REQUEST,
				[
					"content-type" => "application/json; charset=utf-8"
				]
				, json_encode(['massage' => $exception->getMessage()])
			);
		}
	}

	private function actionHistoricalExchange()
	{

		var_dump('Historical exchange');
		return $this->sendRequest(
			$this->currencyFrom,
			$this->currencyTo,
			$this->amount,
			$this->API->getUrlCurrencyHistoricalExchange($this->date,$this->currencyFrom),
			$this->date
		);

	}

	private function actionExchange()
	{
		var_dump('exchange');
		return $this->sendRequest(
			$this->currencyFrom,
			$this->currencyTo,
			$this->amount,
			$this->API->getUrlCurrencyExchange($this->currencyFrom)
		);
	}

	private function sendRequest($from, $to, $amount,$url, $date = NULL )
	{

		return call(function () use ($url, $amount, $to, $from, $date) {
			$deferred = new Deferred();
			$promise = $deferred->promise();
			$client = new DefaultClient();
			$promise = $client->request($url);
			$response = yield $promise;
			$body = yield $response->getBody();
			$jsonResponse = json_decode($body, TRUE);

			$responseDate = [
				'currency_from' => $from,
				'currency_to' => $to,
				'amount' => $amount,
				'result' => CurrencyCalculateHelper::calculate($jsonResponse['rates'], $to, $amount),
				'rate' => CurrencyCalculateHelper::getExchangeRate($jsonResponse['rates'], $to),
				'date' => ConvertHelper::timeStampToDate($jsonResponse['timestamp'])
			];


			return new Response(
				Status::OK,
				[
					"content-type" => "application/json; charset=utf-8"
				]
				, json_encode($responseDate
			));
		});
	}

}