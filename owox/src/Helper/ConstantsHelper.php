<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:59
 */

namespace src\Helper;


class ConstantsHelper
{

	public const METHOD_NOT_ALLOW = 'Method not allow';
	public const PAGE_NOT_FOUND = 'Page not found';
	public const BAD_REQUEST = 'Bad request';
}