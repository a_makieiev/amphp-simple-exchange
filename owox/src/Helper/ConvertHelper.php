<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 15.04.18
 * Time: 0:16
 */

namespace src\Helper;


class ConvertHelper
{
	public static function timeStampToDate(int $timestamp){
		return date('Y-m-d', $timestamp);
	}

}