<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:46
 */

namespace src\Helper;


use Amp\Http\Server\Request;

class RequestHelper
{
	public static function getParams(Request $request)
	{
		$parts = parse_url($request->getUri());
		parse_str($parts['query'], $query);
		return $query;
	}
}