<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 15.04.18
 * Time: 0:10
 */

namespace src\Helper;


class CurrencyCalculateHelper
{
	public static function calculate($rates, $to, $amount)
	{

		foreach ($rates as $attributeKey => $rate) {
			if ($attributeKey == $to) {
				return $rate * $amount;
			}
		}
	}

	public static function getExchangeRate(array $rates, $to)
	{
		foreach ($rates as $attributeKey => $rate) {
			if ($attributeKey == $to) {
				return $rate;
			}
		}
	}
}