<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:13
 */

namespace src\Routing;


use Amp\Http\Server\Request;
use src\Action\CurrencyAction;
use src\Exceptions\ExceptionsResponse;
use function Amp\call;

class Routing
{

	public static function main(Request $request)
	{
		switch ($request->getUri()->getPath()) {
			case '/currency':
				{
					$response = call(function () use ($request) {
						$currencyAction = new CurrencyAction($request);
						return $currencyAction->run();
					});
					return yield $response;

				}
			default:
				{
					return ExceptionsResponse::pageNotFound('Page not found');
					break;
				}
		}
	}
}