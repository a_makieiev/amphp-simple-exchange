<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 14.04.18
 * Time: 23:29
 */

namespace src\Interfaces;


interface ExceptionsResponseInterfaces
{
	public static function badRequest($massage);
	public static function methodNotAllow($massage);
	public static function serverError($massage);
	public static function pageNotFound($massage);
}